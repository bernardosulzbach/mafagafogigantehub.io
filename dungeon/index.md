---
layout: page
title: Dungeon
tags: [sidebar]
---
# What is it?

Dungeon is a text-based RPG developed by me (with some help of awesome people) using Java SE 6.
It is licensed under GNU GPLv3.

# Screenshots

[Click here to go to the screenshot gallery]({{ site.baseurl }}/dungeon/screenshots/).

# Some links

[Latest compiled release](https://github.com/mafagafogigante/dungeon/releases/latest){:target="_blank"} on GitHub.

*If you want a newer release and you simply can't build the sources on your own, feel free to request me a build via e-mail.*

[Dungeon issue tracker](https://github.com/mafagafogigante/dungeon/issues){:target="_blank"}.
You can use the issue tracker to report bugs or leave me your suggestions and questions.

[Dungeon Java Style]({{ site.baseurl }}/dungeon/style){:target="_blank"}.

[Travis CI](https://travis-ci.org/mafagafogigante/dungeon){:target="_blank"} page.

# Requirements

## Runtime Requirements
The only thing you need to run a release is Java (SE 6 or newer).

## Development Requirements
To clone and compile from source you will need:

+ Git;
+ Java SE Development Kit (6 or newer too);
+ Maven 3.

# Foreword

## For players

> I hope this game helps you waste your time in an entertaining way.

## For developers

> I hope this project helps you become a more competent programmer.

# Acknowledgements

[GitHub](https://github.com){:target="_blank"}
for providing free website hosting and our issue tracker.

[Travis CI](https://travis-ci.org/) for providing free CI testing.

# License
This game is licensed under GNU GPLv3.
