---
layout: page
title: Database Design
tags: [sidebar]
---

# Summary
+ [Views](/database-design/views/)
+ [Indexes](/database-design/indexes/)
+ [Compound queries](/database-design/compound-queries/)
