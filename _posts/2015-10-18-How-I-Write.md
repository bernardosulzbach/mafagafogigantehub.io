---
layout: post
title: How I Write
---

This very short post was inspired by [this post by Leonard
Lamprecht](https://leo.im/notes/how-i-write-and-how-i-do-not/).

I too believe that text is a very powerful mean of communication. Most
of the ideas I want to share through this website are easily expressed
with words and – maybe – one or two helpful diagrams. However, in
the end, some algorithms – even if very well explained – need a
picture in order to be completely understood by the average programmer.

Nevertheless, I disagree with “And in the end, the shared information
is even better and more colourful than the equivalent video could ever
be” as I think that some information is better transmitted through
videos, but that is just my opinion.

Therefore, this is how **I** write. All the ideas are expressed
textually, but there is also a diagram here and there to complement the
explanation.
