---
layout: page
title: About
tags: [sidebar]
---

# This website

This is my general-purpose personal website and blog.

My main goal is to create a set of good and useful pages and posts about a wide
variety of topics, with special focus on computer science and software
engineering.

If you would like to make any changes to a post, either e-mail me your
suggestion or open a pull request (or issue) on GitHub and I will analyse your
idea.

Old posts are not moved to the "top" when they are edited, therefore some posts
created months (or even years) ago may be more up-to-date than the "newest"
post.

# Contact information

You can e-mail me at <a
href="mailto:mafagafogigante@gmail.com">mafagafogigante@gmail.com</a>

# My public repositories

All my public repositories are either on GitHub or Bitbucket.

[This](https://github.com/mafagafogigante){:target="_blank"} is my GitHub
profile.  [This](https://bitbucket.org/mafagafogigante){:target="_blank"} is my
Bitbucket profile.
