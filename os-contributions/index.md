---
layout: page
title: Open Source Contributions
tags: [sidebar]
---
This is a list of all my contributions to open source projects.

+ Translated [DB Browser for SQLite](https://github.com/sqlitebrowser/sqlitebrowser) to Brazilian Portuguese.
+ Spelling corrections on [Grimoire](https://github.com/ephe/grimoire).
+ Code clean up, refactorings, and suggestions on [java-design-patterns](https://github.com/iluwatar/java-design-patterns).
+ Code clean up in [python-mysql-replication](https://github.com/noplay/python-mysql-replication).
+ Fixed typos and reworded pages in the [Travis CI documentation](https://github.com/travis-ci/docs-travis-ci-com).
